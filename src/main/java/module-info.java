module com.example.mylovelyfxapp {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.example.mylovelyfxapp to javafx.fxml;
    exports com.example.mylovelyfxapp;
}