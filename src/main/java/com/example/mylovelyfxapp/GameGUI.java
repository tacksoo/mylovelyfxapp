package com.example.mylovelyfxapp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GameGUI extends Application  {

    @Override
    public void start(Stage stage) throws Exception {
        BorderPane bp = new BorderPane();
        GridPane pane = new GridPane();
        pane.setHgap(50);
        pane.setVgap(10);
//        Label titleLabel = new Label("Title");
//        TextField titleTF = new TextField();
//        pane.add(titleLabel, 1, 1);
//        pane.add(titleTF, 2, 1);
        Label [] labels = new Label[6];
        String [] labelNames = {"Title", "System", "Developer", "Category", "Rating", "Year"};
        for (int i = 0; i < labels.length; i++) {
            labels[i] = new Label(labelNames[i]);
            pane.add(labels[i], 1 , 1 + i);
        }
        // do the same for TextField's
        TextField [] textfields = new TextField[6];
        for (int i = 0; i < textfields.length; i++) {
            textfields[i] = new TextField();
            pane.add(textfields[i], 2 , 1 + i);
        }

        Button saveButton = new Button("Save");
        pane.add(saveButton, 1, 7);
        saveButton.setOnAction( e -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("The game has been saved!");
            alert.show();
        });
        Button cancelButton = new Button("Cancel");
        pane.add(cancelButton, 2, 7 );

        bp.setTop(new Label("Block Blaster Inc."));
        bp.setCenter(pane);
        Scene scene = new Scene(bp,400,250);
        stage.setScene(scene);
        stage.setTitle("BlockBlaster Inc");
        stage.show();
    }
}
